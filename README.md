
#Module 01 "Test des stagiaires"#

Consignes : 

 - Créer un projet dans la technologie de votre choix
 - Charger le fichier `membres.json`
 - Afficher la liste des membres dans un tableau
 - Ajouter un champ pour filtrer par `pole` la liste des membres
 - Ajouter un champ de recherche sur le champ `displayname` 
 - Styliser en CSS le tableau des membres et les champs

Contraintes : 

 - Les actions de filtre et de recherche doivent être asynchrones. La page ne doit pas nécéssité d'être rafraichie intégralement.
 - Le projet doit contenir au moins un fichier CSS et un fichier JS de votre composition.
 - Il vous est possible d'intégrer des bibliothèques à votre projet en expliquant votre choix.


